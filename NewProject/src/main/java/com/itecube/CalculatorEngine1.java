package com.itecube;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class CalculatorEngine1 implements ActionListener {
    Calculator1 parent;

    char selectedAction = ' ';
    double currentResult = 0;

    public CalculatorEngine1(Calculator1 parent) {
        this.parent = parent;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        JButton clickButton = (JButton) e.getSource();
        String disFieldText = parent.displayField.getText();

        double displayValue = 0;

        if (!"".equals(disFieldText)) {
            displayValue = Double.parseDouble(disFieldText);
        }

        Object src = e.getSource();

        if (src == parent.buttonPlus) {
            selectedAction = '+';
            currentResult = displayValue;
            parent.displayField.setText("");
        } else if (src == parent.buttonMinus) {
            selectedAction = '-';
            currentResult = displayValue;
            parent.displayField.setText("");
        } else if (src == parent.buttonDivide) {
            selectedAction = '/';
            currentResult = displayValue;
            parent.displayField.setText("");
        } else if (src == parent.buttonMultiply) {
            selectedAction = '*';
            currentResult = displayValue;
            parent.displayField.setText("");
        } else if (src == parent.buttonPlMn) {
            currentResult = displayValue;
            parent.displayField.setText("");
            currentResult = currentResult * -1;
            parent.displayField.setText("" + currentResult);
        } else if (src == parent.buttonC) {
            currentResult = displayValue;
            parent.displayField.setText("");
            currentResult = currentResult * 0;
            parent.displayField.setText("" + currentResult);
        } else if (src == parent.buttonEqual) {
            if (selectedAction == '+') {
                currentResult += displayValue;
                parent.displayField.setText("" + currentResult);
            } else if (selectedAction == '-') {
                currentResult -= displayValue;
                parent.displayField.setText("" + currentResult);
            } else if (selectedAction == '/') {
                currentResult /= displayValue;
                parent.displayField.setText("" + currentResult);
            } else if (selectedAction == '*') {
                currentResult *= displayValue;
                parent.displayField.setText("" + currentResult);
            }
        } else {
            String clickButtonLabel = clickButton.getText();
            parent.displayField.setText(disFieldText + clickButtonLabel);
        }
    }
}



